global main
extern printf

section .text
main:
    push        rbx
    mov         rcx, 90
    xor         rax, rax
    xor         rbx, rbx
    inc         rbx
print:
    push        rax
    push        rcx
    lea         rdi, [rel format]
    mov         rsi, rax
    xor         rax, rax
    call        printf
    pop         rcx
    pop         rax
    mov         rdx, rax
    mov         rax, rbx
    add         rbx, rdx
    dec         rcx
    jnz         print
    pop         rdx
    ret

section .data
    format:     db "%20ld", 10, 0
