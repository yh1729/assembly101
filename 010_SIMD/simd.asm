global add_4_float

section .text
add_4_float:
    movdqa      xmm0, [rdi]
    movdqa      xmm1, [rsi]
    addps       xmm0, xmm1
    movdqa      [rdi], xmm0
    ret
