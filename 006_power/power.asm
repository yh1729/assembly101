global main
extern printf
extern puts
extern atoi

section .text
main:
    push        r12
    push        r13
    push        r14
    cmp         rdi, 3
    jne         error_bad_argument
    mov         r12, rsi
    mov         rdi, [r12 + 16]
    call        atoi
    cmp         eax, 0
    jl          error_negative_exponent
    mov         r13d, eax
    mov         rdi, [r12 + 8]
    call        atoi
    mov         r14d, eax
    mov         eax, 1
check:
    test        r13d, r13d
    jz          answer
    imul        eax, r14d
    dec         r13d
    jmp         check
answer:
    lea         rdi, [rel answer_format]
    movsxd      rsi, eax
    xor         rax, rax
    call        printf
    jmp         done
error_bad_argument:
    lea         rdi, [rel error_bad_argument_format]
    call        puts
    jmp         done
error_negative_exponent:
    lea         rdi, [rel error_negative_exponent_format]
    call        puts
done:
    pop         r14
    pop         r13
    pop         r12
    ret

section .data
    answer_format:
                db "%d", 10, 0
    error_bad_argument_format:
                db "Requires exactly two qrguments", 10, 0
    error_negative_exponent_format:
                db "The exponent may not be negative", 10, 0
