global max_of_3

section .text
max_of_3:
    mov         rax, rdi
    cmp         rax, rsi
    cmovl       rax, rsi
    cmp         rax, rdx
    cmovl       rax, rdx
    ret
