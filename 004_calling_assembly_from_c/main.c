#include <stdio.h>
#include <inttypes.h>

int64_t max_of_3(int64_t, int64_t, int64_t);

int main() {
    printf("%ld\n", max_of_3(1, 2, 3));
    return 0;
}
