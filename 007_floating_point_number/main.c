#include <stdio.h>
#include <inttypes.h>

double sum_of_double(double[], uint64_t);

int main() {
    double test[] = {
        40.5, 26.7, 21.9, 1.5, -40.5, -23.4
    };
    printf("%20.7f\n", sum_of_double(test, 6));
    return 0;
}
