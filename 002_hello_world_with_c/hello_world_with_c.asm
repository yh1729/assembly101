global main
extern puts

section .text
main:
    push        rbx
    lea         rdi, [rel message]
    call        puts
    pop rbx
    ret
section .data
    message:    db "hello world, with C", 0
