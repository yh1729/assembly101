global main
extern atoi
extern printf

section .text
main:
    dec         rdi
    jz          no_input
    mov         [rel count], rdi
accumulate:
    push        rdi
    push        rsi
    mov         rdi, [rsi + rdi * 8]
    sub         rsp, 8
    call        atoi
    add         rsp, 8
    pop         rsi
    pop         rdi
    add         [rel sum], rax
    dec         rdi
    jnz         accumulate
average:
    cvtsi2sd    xmm0, [rel sum]
    cvtsi2sd    xmm1, [rel count]
    divsd       xmm0, xmm1
    lea         rdi, [rel format]
    mov         rax, 1
    sub         rsp, 8
    call        printf
    add         rsp, 8
    ret
no_input:
    lea         rdi, [rel error]
    xor         rax, rax
    sub         rsp, 8
    call        printf
    add         rsp, 8
    ret

section .data
    count:      dq 0.0
    sum:        dq 0.0
    format:     db "%g", 10, 0
    error:      db "There are no command line arguments to average", 10, 0
