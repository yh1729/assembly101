global main

section .text
main:
    mov         rax, 0x02000004
    mov         rdi, 1
    lea         rsi, [rel message]
    mov         rdx, 12
    syscall
    mov         rax, 0x02000001
    xor         rdi, rdi
    syscall

section .data
    message:    db "hello world", 10
