global factorial

section .text
factorial:
    cmp         rdi, 1
    jnbe        level_1
    mov         rax, 1
    ret
level_1:
    push        rdi
    dec         rdi
    call        factorial
    pop         rdi
    imul        rax, rdi
    ret
